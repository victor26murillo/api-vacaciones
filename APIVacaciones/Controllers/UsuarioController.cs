﻿using APIVacaciones.Models;
using APIVacaciones.Servicios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace APIVacaciones.Controllers
{
    public class UsuarioController : ApiController
    {
        private UsuarioServicios servicio;
        public UsuarioController()
        {
            servicio = new UsuarioServicios();
        }

        [HttpGet]
        public IHttpActionResult Get() {
            var lista_usuario = servicio.GetAll();
            if (lista_usuario != null) {
                return Ok(lista_usuario);
            }
            return NotFound();
        }

        [HttpPost]
        public IHttpActionResult Post(UsuarioModel model) {
            if (model != null)
            {
                model.fecha_creacion = DateTime.Now;
                return Ok(servicio.Insert(model));
            }
            else {
                return NotFound();
            }
        }


    }
}
