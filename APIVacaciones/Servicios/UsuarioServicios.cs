﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using APIVacaciones.Interfaces;
using APIVacaciones.Models;
using APIVacaciones.Modelo;
using AutoMapper;

namespace APIVacaciones.Servicios
{
    
    public class UsuarioServicios : IBaseService<UsuarioModel>
    {


        public List<UsuarioModel> GetAll()
        {
              using(var contexto = new vacacionesEntities())
                {
                  var queryResult=contexto.usuario.Select(u=>u).ToList();
                  return Mapper.Map<List<UsuarioModel>>(queryResult);
                }
}
        

        public UsuarioModel GetByID(int id)
        {
            throw new NotImplementedException();
        }

        public bool Insert(UsuarioModel entidad)
        {
           using(var contexto =new vacacionesEntities()){
               contexto.usuario.Add(Mapper.Map<usuario>(entidad));
               contexto.SaveChanges();
               return true;
           }
        }

        public bool Update(UsuarioModel entidad)
        {
            throw new NotImplementedException();
        }

        public bool Deleted(int id)
        {
            throw new NotImplementedException();
        }
    }
}