﻿using APIVacaciones.Modelo;
using APIVacaciones.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APIVacaciones.Helpers
{
        public class MapperConfig
        {
            public static void Configure()
            {
                Mapper.Initialize(cfg =>
                {
                    cfg.CreateMap<UsuarioModel, usuario>();
                    cfg.CreateMap<usuario, UsuarioModel>();
                   
                });

            }
        }
}