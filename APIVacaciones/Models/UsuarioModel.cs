﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APIVacaciones.Models
{
    public class UsuarioModel
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public string segundo_nombre { get; set; }
        public string segundo_apellido { get; set; }
        public string cedula { get; set; }
        public Nullable<System.DateTime> fecha_nacimiento { get; set; }
        public string correo { get; set; }
        public Nullable<System.DateTime> fecha_creacion { get; set; }
        public Nullable<int> tipo_empleado { get; set; }
        public string password { get; set; }
        public Nullable<bool> status { get; set; }
        public Nullable<bool> reset_password { get; set; }
    }
}