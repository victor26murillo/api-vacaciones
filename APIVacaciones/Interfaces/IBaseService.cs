﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIVacaciones.Interfaces
{
    public interface IBaseService<T>
        
    {
         List<T> GetAll();

         T GetByID(int id);

         bool Insert(T entidad);

         bool Update(T entidad);

         bool Deleted(int id);

    }
}
